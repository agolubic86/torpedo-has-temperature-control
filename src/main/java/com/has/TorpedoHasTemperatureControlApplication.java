package com.has;

import com.has.mqttHelper.MqttTemperatureCallback;
import com.has.mqtt.MqttUtility;
import com.has.mqttHelper.TemperatureMqttTopicProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;

@SpringBootApplication
@EnableDiscoveryClient
@ComponentScan
@PropertySource({"classpath:/mqtt.properties", "classpath:/mongodb.properties", "classpath:/application.properties"})
public class TorpedoHasTemperatureControlApplication {

	@Bean
	public MqttTemperatureCallback mqttTemperatureCallback() {
		return new MqttTemperatureCallback();
	}

	@Bean
	public MqttUtility mqttUtility() { return new MqttUtility(mqttTemperatureCallback()); }

	public static void main(String[] args) {

		System.setProperty("spring.config.name", "temperature-server");
		ApplicationContext context = SpringApplication.run(TorpedoHasTemperatureControlApplication.class, args);

		MqttUtility mqttUtility = context.getBean(MqttUtility.class);
		TemperatureMqttTopicProperties topicProperties = context.getBean(TemperatureMqttTopicProperties.class);
		mqttUtility.subscribe(topicProperties.getSubscribe());

	}

}

