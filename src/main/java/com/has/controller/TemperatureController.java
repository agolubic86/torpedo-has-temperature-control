package com.has.controller;

import com.has.constants.Room;
import com.has.domain.Temperature;
import com.has.services.TemperatureManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by torpedo on 01.12.17..
 */

@RestController
public class TemperatureController {

    @Value("${build.version}")
    private String buildVersion;

    @Value("${build.timestamp}")
    private String buildTimestamp;

    @Autowired
    private TemperatureManager temperatureManager;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home() {
        return "TEMPERATURE SERVICE. Version: " + buildVersion + "- Build@" + buildTimestamp;
    }

    @RequestMapping(value = "temperatures", method = RequestMethod.GET)
    public List<Temperature> findTemperatures() {
        return temperatureManager.fetchTemperatures();
    }

    @RequestMapping(value = "/temperatures/{temperatureId}/rooms/{roomId}", method = RequestMethod.GET)
    public Temperature fetchTemperature(@PathVariable("temperatureId") final Integer temperatureId,
                                        @PathVariable("roomId") final Integer roomId) {
        Room room = Room.getByValue(roomId);
        return temperatureManager.findTemperature(temperatureId, room);
    }

    @RequestMapping(value = "temperatures/rooms/{roomId}", method = RequestMethod.GET)
    public Temperature fetchTemperatureByRoomId(@PathVariable("roomId") final Integer roomId) {
        return temperatureManager.findTemperatureByRoom(roomId);
    }

}
