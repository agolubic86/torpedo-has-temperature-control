package com.has.mqttHelper;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.has.domain.Temperature;
import com.has.mqtt.CustomMqttCallback;
import com.has.services.TemperatureManager;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by torpedo on 18.02.18..
 */
@Component
public class MqttTemperatureCallback extends CustomMqttCallback {

    private static final Logger LOGGER = LoggerFactory.getLogger(MqttTemperatureCallback.class);

    @Autowired
    private TemperatureManager temperatureManager;

    @Override
    public void messageArrived(String topic, MqttMessage message) throws Exception {
        try {
            LOGGER.info("Mapping MQTT message to object. Message={}", message);
            ObjectMapper mapper = new ObjectMapper();
            Temperature temperature = mapper.readValue(message.getPayload(), Temperature.class);

            temperatureManager.registerTemperature(temperature);

        }catch (JsonMappingException jme) {
            LOGGER.error("Error mapping message={} to object.", message, jme);
        }catch (JsonParseException jpe) {
            LOGGER.error("Error parsing message={}", message, jpe);
        }catch (Exception e) {
            LOGGER.error("Error with MQTT callback.", e);
        }
    }
}
