package com.has.services;

import com.has.constants.Room;
import com.has.domain.Temperature;
import com.has.domain.TemperatureError;
import com.has.mqtt.MqttUtility;
import com.mongodb.MongoException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by torpedo on 17.02.18..
 */
@Service
public class TemperatureManagerImpl implements TemperatureManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(TemperatureManagerImpl.class);
    public static final String TEMPERATURE_ID_PARAM = "temperatureId";
    public static final String ROOM_PARAM = "room";
    public static final String READOUT_TIME_PARAM = "readoutTime";

    @Autowired
    private MqttUtility mqttUtility;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public Temperature registerTemperature(final Temperature temperature) {
        try{
            if(temperatureSensorExists(temperature))
                throw new TemperatureError(String.format("Temperature with id = %s in room = %s already registered.", temperature.getId(), temperature.getRoom().name()));

            mongoTemplate.insert(temperature);

            return temperature;
        }catch (MongoException e) {
            throw new TemperatureError("Error registering new temperature.", e);
        }
    }


    @Override
    public Temperature findTemperature(final Integer temperatureId, final Room room) {
        return mongoTemplate.findOne(Query.query(Criteria.where(TEMPERATURE_ID_PARAM).is(temperatureId).and(ROOM_PARAM).is(room)).with(new Sort(Sort.Direction.DESC, READOUT_TIME_PARAM)), Temperature.class);
    }

    @Override
    public List<Temperature> fetchTemperatures() {
        try{
            LOGGER.info("Fetching temperatures!");
            return mongoTemplate.findAll(Temperature.class);
        }catch (MongoException me) {
            throw new TemperatureError("Error finding temperature data.", me);
        }
    }

    @Override
    public Temperature findTemperatureByRoom(final Integer roomId) {
        return mongoTemplate.findOne(Query.query(Criteria.where(ROOM_PARAM).is(Room.getByValue(roomId))).with(new Sort(Sort.Direction.DESC, READOUT_TIME_PARAM)), Temperature.class);
    }

    private boolean temperatureSensorExists(final Temperature temperature) {
        return mongoTemplate.exists(Query.query(Criteria.where(TEMPERATURE_ID_PARAM).is(temperature.getId()).and(ROOM_PARAM).is(temperature.getRoom())), Temperature.class);
    }
}
