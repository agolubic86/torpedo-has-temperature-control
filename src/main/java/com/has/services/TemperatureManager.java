package com.has.services;


import com.has.constants.Room;
import com.has.domain.Temperature;

import java.util.List;

/**
 * Created by torpedo on 30.01.18..
 */
public interface TemperatureManager {

    Temperature registerTemperature(Temperature temperature);

    Temperature findTemperature(Integer temperatureId, Room room);

    List<Temperature> fetchTemperatures();

    Temperature findTemperatureByRoom(Integer roomId);
}
