package com.has.domain;

import com.has.constants.Room;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by torpedo on 01.12.17..
 */

@Document(collection = "temperature")
public class Temperature {

    @Id
    private String id;
    private Integer temperatureId;
    private Room room;
    private BigDecimal temperatureValue;
    private BigDecimal humidityValue;
    private Date readoutTime;

    public Temperature() {
        this.readoutTime = new Date();
    }

    public Temperature(Integer temperatureId, Room room) {
        this.temperatureId = temperatureId;
        this.room = room;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getTemperatureId() {
        return temperatureId;
    }

    public void setTemperatureId(Integer temperatureId) {
        this.temperatureId = temperatureId;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public BigDecimal getTemperatureValue() {
        return temperatureValue;
    }

    public void setTemperatureValue(BigDecimal temperatureValue) {
        this.temperatureValue = temperatureValue;
    }

    public BigDecimal getHumidityValue() {
        return humidityValue;
    }

    public void setHumidityValue(BigDecimal humidityValue) {
        this.humidityValue = humidityValue;
    }

    public Date getReadoutTime() {
        return readoutTime;
    }

    public void setReadoutTime(Date readoutTime) {
        this.readoutTime = readoutTime;
    }

    @Override
    public String toString() {
        return "Temperature: id=" + temperatureId;
    }
}
