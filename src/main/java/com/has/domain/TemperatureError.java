package com.has.domain;

/**
 * Created by torpedo on 01.02.18..
 */
public class TemperatureError extends RuntimeException {

    public TemperatureError(String errorMessage) {
        super(errorMessage);
    }

    public TemperatureError(String errorMessage, Throwable th) {
        super(errorMessage, th);
    }
}
